QT             += widgets printsupport uitools
TEMPLATE       = app
CONFIG         += no_keywords release
INCLUDEPATH    += /usr/local/include
LIBS           += -lecl -leql5 -L.. -L/usr/local/lib
TARGET         = eql5
DESTDIR        = ../
OBJECTS_DIR    = ./tmp/
MOC_DIR        = ./tmp/

QMAKE_RPATHDIR = /usr/lib
target.path    = /usr/bin
INSTALLS       = target

win32 {
    include(windows.pri)
    CONFIG += console
}

osx {
    CONFIG -= app_bundle
}

SOURCES += main.cpp

RESOURCES = eql_exe.qrc
